package benutzerverwaltung;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Benutzerverwaltung {

    public static void main(String[] args) throws IOException {
        BenutzerverwaltungV20.start();
    }
}

class BenutzerverwaltungV20{
    private static BenutzerListe benutzerListe;

    public static void start() throws IOException {
        benutzerListe = new BenutzerListe();

        dateiEinlesen();
        // benutzerListe.insert(new Benutzer("Paula", Crypto.encrypt("paula".toCharArray())));
        // benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        // benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));
        // benutzerListe.insert(new Benutzer("Admin", Crypto.encrypt("Hallo123".toCharArray())));
        schreibeBenutzerInDatei();

        Scanner tastatur = new Scanner(System.in);


        boolean systemLäuft = true;
        while(systemLäuft) {
            int auswahl = Helper.RequestInputWithOptions("Willkommen! Bitte wähle eine Option:", new String[] {"Anmelden", "Registrieren"}, true, true, true);
            int failCount = 0;

            switch(auswahl) {
                case 1: System.out.print("Name: ");
                    String inputName = tastatur.next();
                    System.out.print("Passwort: ");
                    String inputPasswort = tastatur.next();
                    if (authenticate(inputName, Crypto.encrypt(inputPasswort.toCharArray()))) {
                        failCount = 0;
                        System.out.println("Hallo " + inputName + "! Sie sind angemeldet.");
                        // Arbeitsumgebung des Benutzers starten.
                        // ...
                        // Benutzer hat sich abgemeldet.
                        System.out.println("Auf Wiedersehen.");
                        System.out.print("System herunterfahren? [j/n] ");
                        systemLäuft = !tastatur.next().equals("j");
                    } else {
                        System.out.println("Name oder Passwort falsch.");
                        failCount = failCount + 1;
                        if(failCount == 3) {
                            System.out.println("Fehlerlimit erreicht. Das System wird nun heruntergefahren!");
                            systemLäuft = false;
                        }
                    }
                    break;
                //Registrierung
                case 2:
                    System.out.print("Name: ");
                    String registrierungName = tastatur.next();
                    System.out.print("Passwort: ");
                    String registrierungPasswort = tastatur.next();
                    System.out.print("Passwort wiederholen: ");
                    String registrierungPasswortWiederholung = tastatur.next();
                    if(registrierungPasswort == registrierungPasswortWiederholung && benutzerListe.getBenutzer(registrierungName) == null) {
                        benutzerListe.insert(new Benutzer(registrierungName, Crypto.encrypt(registrierungPasswort.toCharArray())));
                        System.out.print("Der Benutzer wurde registriert!");
                    }
                    break;

            }
        }
    }

    public static boolean authenticate(String name, char[] cryptoPw) {
        Benutzer b = benutzerListe.getBenutzer(name);
        if(b != null) {
            if(b.hasPasswort(cryptoPw)){
                return true;
            }
        }
        return false;
    }

    public static void dateiEinlesen(){
        try{
            String[] benutzerStrings;
            FileReader reader = new FileReader("benutzer.txt");
            BufferedReader inBuffer = new BufferedReader(reader);

            String line = inBuffer.readLine();

            while (line != null)
            {
                System.out.println(line);
                benutzerStrings = line.split("%_%");
                benutzerListe.insert(new Benutzer(benutzerStrings[0], benutzerStrings[1].toCharArray()));
                line = inBuffer.readLine();
            }
        }
        catch(IOException e){
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void schreibeBenutzerInDatei() {
        try {
            File file = new File("benutzer.txt");
            if (file.createNewFile()) {
                System.out.println("Benutzerdatei erstellt: " + file.getName());
            }

            FileWriter writer = new FileWriter("benutzer.txt");

            writer.write(benutzerListe.select());
            writer.close();

            System.out.println("Benutzer in Benutzerdatei geschrieben.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}

class BenutzerListe {
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }

    public Benutzer getBenutzer(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b;
            }
            b = b.getNext();
        }
        return null;
    }

    public void insert(Benutzer b) {
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null) {
            first = last = b;
        }
        else {
            last.setNext(b);
            last = b;
        }
    }

    public String select() {
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }

    public String select(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name) {
        // ...
        return true;
    }
}

class Benutzer {
    private String name;
    private char[] passwort;  // Verschlüsselt!

    private Benutzer next;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public boolean hasPasswort(char[] cryptoPw){
        return Arrays.equals(this.passwort, cryptoPw);
    }

    public String toString(){
        String s = "";
        s += name + "%_%";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}

class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }
}

class Helper {

    public static void PrintLine() {
        System.out.println("--------------------");
    }
    public static void RequestKeyInputToContinue() throws IOException {
        System.out.println("Beliebige Taste drücken um fortzufahren.");
        System.in.read();
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static int RequestInputWithOptions(String header, String options[], Boolean clearBeforeOutput, Boolean printTopLine, Boolean printBottomLine) throws IOException {
        if (clearBeforeOutput) {
            System.out.print("\033[H\033[2J");
            System.out.flush();
        }

        if (printTopLine)
            PrintLine();
        System.out.println(header);

        int i = 1;

        for(String option : options) {
            System.out.println("[" + i + "] " + option);
            i++;
        }

        if (printBottomLine)
            PrintLine();
        System.out.print("Auswahl: ");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            var input = scanner.next();
            if(isInteger(input)) {
                int inputAsNumber = Integer.parseInt(input);
                if (inputAsNumber > options.length || inputAsNumber == 0) {
                    System.out.println();
                    System.out.print("Ungültige Eingabe! Bitte erneut versuchen: ");
                } else
                    return inputAsNumber;
            }
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public static int RequestIntInput(String question, Boolean clearBeforeOutput) throws IOException {
        if (clearBeforeOutput) {
            Runtime.getRuntime().exec("cls");
        }
        System.out.print(question + ": ");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            var input = scanner.next();

            if (isInteger(input)) {
                System.out.println();
                System.out.print("Ungültige Eingabe! Bitte erneut versuchen: ");
            } else
                return Integer.parseInt(input);
        }
    }
}
