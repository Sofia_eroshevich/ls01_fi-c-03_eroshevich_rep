import java.util.Scanner;

public class MonateAufgabe {
    public static void main(String[] args){

        Scanner uInput = new Scanner(System.in);

        System.out.println("Bitte geben Sie eine Zahl zwischen 1 und 12 ein");
        int month= uInput.nextInt();
        getMonth(month);
        System.out.println("Der Monat ist: " + getMonth(month));


    }

    public static String getMonth (int zahl){
        if(zahl == 1){
        return "Januar";
        }
        else if(zahl == 2){
            return "Februar";
        }
        else if(zahl == 3){
            return "März";
        }
        else if(zahl == 4){
            return "April";
        }
        else if(zahl == 5){
            return "Mai";
        }
        else if(zahl == 6){
            return "Juni";
        }
        else if(zahl == 7){
            return "Juli";
        }
        else if(zahl == 8){
            return "August";
        }
        else if(zahl == 9){
            return "September";
        }
        else if(zahl == 10){
            return "Oktober";
        }
        else if(zahl == 11){
            return "November";
        }
        else{
            return "Dezember";
        }


    }
}

