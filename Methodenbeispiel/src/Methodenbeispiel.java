import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class Methodenbeispiel {
    public static void main(String[] args) {

        int zahl1 = 5;
        int zahl2 = 7;
        int zahl3 = 9;

        int erg = min(zahl1, zahl2);
        int erg2 = max(zahl1, zahl2, zahl3);

        System.out.println("Ergebnis: " + erg);
        System.out.println("Ergebnis: " + erg2);
    }

    public static int min(int zahl1, int zahl2) {
        if (zahl1 < zahl2) {
            return zahl1;
        } else {
            return zahl2;
        }
    }

    public static int max(int zahl1, int zahl2, int zahl3) {
        if (zahl1 > zahl2 && zahl1 > zahl3) {
            return zahl1;
        } else if (zahl2 > zahl1 && zahl2 > zahl3) {
            return zahl2;
        } else {
            return zahl3;
        }
    }
}