import java.util.Scanner;

public class Steuersatz {

    public static void main(String[] args){
        Scanner uInput = new Scanner(System.in);

        System.out.println("Wird der Betrag voll versteuert? ");
        String yesOrNo= uInput.next();
        boolean vollerSteuersatz;
        if(yesOrNo.equals("y")){
            vollerSteuersatz = true;
        } else{
            vollerSteuersatz = false;
        }

        System.out.println("Bitte geben Sie den zu versteuernden Betrag an: y/n ");
        double zahl= uInput.nextDouble();

        steuern(vollerSteuersatz,zahl);
        System.out.println("Bitte geben Sie den zu versteuernden Betrag an: "+ steuern(vollerSteuersatz, zahl));
    }
    public static double steuern(boolean vollerSteuersatz, double zahl){

        if(vollerSteuersatz){
           double erg = zahl*0.16;
           return erg;
        }
        else{
            double erg2 = zahl*0.07;
            return erg2;
        }
    }
}

