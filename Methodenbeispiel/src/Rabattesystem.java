import java.util.Scanner;

public class Rabattesystem {
    public static void main(String[] args){

        Scanner uInput = new Scanner(System.in);
        System.out.println("Zu zahlender Betrag: ");
        double zahl= uInput.nextInt();
        rabatt(zahl);
        System.out.println("Zu zahlender Betrag (Rabatt bereits abgezogen): "+rabatt(zahl)+ " €");
    }
    public static double rabatt(double zahl){
        if(zahl<= 100){
            return zahl-zahl*0.1;
        }
        else if(zahl>100 && zahl<=500){
            return zahl-zahl*0.15;
        }
        else{
            return zahl-zahl*0.2;
        }
    }
}
