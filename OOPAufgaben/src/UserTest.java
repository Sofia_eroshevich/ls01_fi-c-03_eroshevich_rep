import java.util.Date;

public class UserTest {

    public static void main(String[] args) {

        User referenzUser = new User("referenzUser");
        User testUser1 = new User("User1");
        User testUser2 = new User("User2");

        referenzUser.seteMail("referenzuser@mail.com");
        referenzUser.setPasswort("P4$$w0rD1-2.M3-1337");
        referenzUser.setPermissions((short) 1);
        referenzUser.setLoginStatus(true);

        testUser1.seteMail("user1@mail.com");
        testUser1.setPasswort("ZHBjd$w7561!");
        testUser1.setPermissions((short) 2);
        testUser1.setLoginStatus(false);
        // 	Mon Jan 19 17:11:20 CET 1970
        testUser1.setLastLoginDate(new Date(1613480644));

        testUser2.seteMail("user2@mail.com");
        testUser2.setPasswort("HGjkdc-niwhqwz1");
        testUser2.setPermissions((short) 3);
        testUser2.setLoginStatus(true);

        //test für referenzUser
        if (referenzUser.getName().equals("referenzUser")) {
            System.out.println("Ich heiße referenzUser.");
        } else {
            System.out.println("Ich sollte referenzUser heißen und nicht: " + referenzUser.getName());
        }

        if (referenzUser.geteMail().equals("referenzuser@mail.com")) {
            System.out.println("Meine E-Mail ist referenzuser@mail.com .");
        } else {
            System.out.println("Meine E-Mail sollte referenzuser@mail.com sein und nicht: " + referenzUser.geteMail());
        }

        if (referenzUser.getPasswort().equals("P4$$w0rD1-2.M3-1337")) {
            System.out.println("Ja das P4$$w0rD1-2.M3-1337 ist mein Passwort.");
        } else {
            System.out.println("Mein Passwort sollte P4$$w0rD1-2.M3-1337 sein und nicht: " + referenzUser.getPasswort());
        }

        if (referenzUser.getPermissions() == (short) 1) {
            System.out.println("Meine Permission = 1");
        } else {
            System.out.println("Meine Permission sollte 1 sein und nicht: " + referenzUser.getPermissions());
        }

        if (referenzUser.isLoginStatus()) {
            System.out.println("Ich bin online.");
        } else {
            System.out.println("Ich sollte online sein und nicht: " + referenzUser.isLoginStatus());
        }

        System.out.println("-----------------------------------------------------------------------------------------\n");

        //test für testUser1
        if (testUser1.getName().equals("User1")) {
            System.out.println("Ich heiße User1.");
        } else {
            System.out.println("Ich sollte User1 heißen und nicht: " + testUser1.getName());
        }
        if (testUser1.geteMail().equals("user1@mail.com")) {
            System.out.println("Ich meine E-Mail ist user1@mail.com .");
        } else {
            System.out.println("Ich meine E-Mail sollte user1@mail.com sein und nicht: " + testUser1.geteMail());
        }

        if (testUser1.getPasswort().equals("ZHBjd$w7561!")) {
            System.out.println("Ja das ZHBjd$w7561! ist mein Passwort.");
        } else {
            System.out.println("Mein Passwort sollte ZHBjd$w7561! sein und nicht: " + testUser1.getPasswort());
        }

        if (testUser1.getPermissions() == (short) 2) {
            System.out.println("Meine Permission = 2");
        } else {
            System.out.println("Meine Permission sollte 2 sein und nicht: " + testUser1.getPermissions());
        }

        if (!testUser1.isLoginStatus()) {
            System.out.println("Ich bin offline.");
        } else {
            System.out.println("Ich sollte offline sein und nicht: " + testUser1.isLoginStatus());
        }

        if (testUser1.getLastLoginDate().equals(new Date(1613480644))) {
            System.out.println("Ich war das letzte mal online: " + testUser1.getLastLoginDate());
        } else {
            System.out.println("Ich war eigentlich das letzte mal online um: "+ new Date(1613480644) +" und nicht um: " + testUser1.getLastLoginDate());
        }

        if (testUser1.getLastLoginDateAsString().equals(new Date(1613480644).toString())) {
            System.out.println("Ich war das letzte mal online: " + testUser1.getLastLoginDateAsString());
        } else {
            System.out.println("Ich war eigentlich das letzte mal online um: "+ new Date(1613480644).toString() +" und nicht um: " + testUser1.getLastLoginDateAsString());
        }

        System.out.println("-----------------------------------------------------------------------------------------\n");

        //test für testUser2
        if (testUser2.getName().equals("User2")) {
            System.out.println("Ich heiße User2.");
        } else {
            System.out.println("Ich sollte User2 heißen und nicht: " + testUser2.getName());
        }

        if (testUser2.geteMail().equals("user2@mail.com")) {
            System.out.println("Meine E-Mail ist user2@mail.com .");
        } else {
            System.out.println("Meine E-Mail sollte user2@mail.com sein und nicht: " + testUser2.geteMail());
        }

        if (testUser2.getPasswort().equals("HGjkdc-niwhqwz1")) {
            System.out.println("Ja das HGjkdc-niwhqwz1 ist mein Passwort.");
        } else {
            System.out.println("Mein Passwort sollte HGjkdc-niwhqwz1 sein und nicht: " + testUser2.getPasswort());
        }

        if (testUser2.getPermissions() == (short) 3) {
            System.out.println("Meine Permission = 3");
        } else {
            System.out.println("Meine Permission sollte 3 sein und nicht: " + testUser2.getPermissions());
        }

        if (testUser2.isLoginStatus()) {
            System.out.println("Ich bin online.");
        } else {
            System.out.println("Ich sollte Online sein und nicht: " + testUser2.isLoginStatus());
        }

        System.out.println("-----------------------------------------------------------------------------------------\n");

    }
}
