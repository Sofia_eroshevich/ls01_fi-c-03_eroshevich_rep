import java.util.Date;

public class User {
    private String name;
    private String eMail;
    private String passwort;
    private short permissions;
    private boolean loginStatus;
    private Date lastLoginDate;

    // This is a constructor
    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public short getPermissions() {
        return permissions;
    }

    public void setPermissions(short permissions) {
        this.permissions = permissions;
    }

    public boolean isLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(boolean loginStatus) {
        this.loginStatus = loginStatus;
        if (isLoginStatus()) {
            setLastLoginDate(new Date(System.currentTimeMillis()));
        }
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLastLoginDateAsString() {
        return lastLoginDate.toString();
    }
}
