import java.util.Scanner;

public class aufgabe3 {

    public static void main(String[] Args) {

        char[] chars = new char[5];
        Scanner input = new Scanner(System.in);

        for (int i=0; i < chars.length; i++) {
            System.out.println("Bitte geben sie das " + (i + 1) + ". Zeichen:");
            chars[i] = input.next().trim().charAt(0);
        }

        System.out.println("\nDie Zeichen rückwärts:");
        for (int i=chars.length - 1; i >=0; i--) {
            System.out.println(chars[i]);
        }
    }
}

