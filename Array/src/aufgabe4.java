public class aufgabe4 {

    public static void main(String[] Args) {

        int[] lotto = new int[6];

        System.out.print("[ ");
        for (int i = 0; i < lotto.length; i++) {
            int[] array = {3, 7, 12, 18, 37, 42};

            lotto[i] = array[i];
            System.out.print(lotto[i] + " ");
        }

        System.out.print("]\n");

        for (int i = 0; i < lotto.length; i++) {
            if (lotto[i] == 12) {
                System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
                break;
            } else {
                continue;
            }
        }

        for (int i = 0; i < lotto.length; i++) {
            if (lotto[i] == 13) {
                System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
                break;
            } else {
                if (i == lotto.length - i) {
                    System.out.println("Die Zahl 13 ist in der Ziehung nicht enthalten.");
                }
                continue;
            }
        }
    }
}
