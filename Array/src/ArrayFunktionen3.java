import java.util.Arrays;

public class ArrayFunktionen3 {

    public static int[] reverseArray(int[] feld) {
        int[] neuesFeld = new int[7];
        for(int i = 0; i < feld.length / 2; i++)
        {
            int x = feld[i];
            feld[i] = feld[feld.length - i - 1];
            feld[feld.length - i - 1] = x;
            neuesFeld[i] = feld[i];
        }
        return neuesFeld;
    }

    public static void main(String[] args) {

        int[] array = {2, 65, 34, 7, 2, 98, 82};
        System.out.println(Arrays.toString(reverseArray(array)));
    }
}
