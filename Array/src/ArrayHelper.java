public class ArrayHelper {

    public static String convertArrayToString(int[] zahlen) {
        StringBuilder string = new StringBuilder();

        for (int i = 0; true; i++) {
            try {
                string.append(", " + zahlen[i]);
            } catch (Exception e) {
                break;
            }
        }

        String output = string.toString().substring(2);
        return output;
    }

}
