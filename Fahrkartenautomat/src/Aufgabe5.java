import java.util.Scanner;

public class Aufgabe5 {

        static  double zuZahlenderBetrag;
        static  double eingezahlterGesamtbetrag;
        static  double eingeworfeneMuenze;
        static  double rueckgabebetrag;
        static  int anzahlFahrkarten;
        static  double gesammtBetrag;


        public static void fahrkartenBestellungErfassen(Scanner tastatur){

            System.out.print("Zu zahlender Betrag (EURO): ");
            zuZahlenderBetrag = tastatur.nextDouble();

            // 2.Aufgabe
            System.out.println("Bitte tragen sie die Anzahl der Fahrkarten ein, die Sie erwerden wollen");
            anzahlFahrkarten = tastatur.nextInt();

            if(anzahlFahrkarten >10) {
                anzahlFahrkarten= 1;
                System.out.println("Maximale Anzahl zu erwerbender Fahrkarten überschritten! \nMaximale Anzahl: 10 \nEs wurde nur eine Karte ausgewählt.\n\n");
            }

            gesammtBetrag = zuZahlenderBetrag * anzahlFahrkarten;
            // Aufgabe 1
            System.out.printf("%s %.2f %n","Zu zahlender Betrag (EURO): ", gesammtBetrag);
        }


        public static void fahrkartenBezahlen(Scanner tastatur){

            eingezahlterGesamtbetrag = 0.0;
            while(eingezahlterGesamtbetrag < gesammtBetrag)
            {
                //Aufgabe 1
                System.out.printf("%s %.2f %n","Noch zu zahlen: ", (gesammtBetrag - eingezahlterGesamtbetrag));
                System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
                eingeworfeneMuenze = tastatur.nextDouble();
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }
        }

        public static void warte(){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        public static void fahrkartenAusgabe(){

            // Fahrscheinausgabe
            // -----------------
            System.out.println("\nFahrschein wird ausgegeben");
            for (int i = 0; i < 8; i++)
            {
                System.out.print("=");
                warte();
            }
            System.out.println("\n\n");

        }

        public static void muenzenAusgeben(double betrag, String einheit){
            System.out.println(betrag+einheit);
            rueckgabebetrag -= betrag;
            rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;

        }

        public static void rueckgeldAusgabe(){

            // Rückgeldberechnung und -Ausgabe
            // -------------------------------
            rueckgabebetrag = eingezahlterGesamtbetrag - gesammtBetrag;
            rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            if(rueckgabebetrag > 0.0)
            {
                System.out.println("Der Rückgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
                System.out.println("wird in folgenden Münzen ausgezahlt:");


                while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
                {
                    muenzenAusgeben(2.0," €");
                }
                while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
                {
                    muenzenAusgeben(1.0, " €");
                }
                while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
                {
                    muenzenAusgeben(0.5, " €");
                }
                while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
                {
                    muenzenAusgeben(0.2, " €");
                }
                while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
                {
                    muenzenAusgeben(0.1, " €");
                }
                while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
                {
                    muenzenAusgeben(0.05, " €");
                }
            }

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.");
        }

        public static void main(String[] args) {

            Scanner tastatur = new Scanner(System.in);
            fahrkartenBestellungErfassen(tastatur);
            fahrkartenBezahlen(tastatur);
            fahrkartenAusgabe();
            rueckgeldAusgabe();
        }
}

