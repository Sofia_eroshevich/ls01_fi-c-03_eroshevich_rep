import java.util.Scanner;

public class Aufgabe12zusatz {
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);
        
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        double eingeworfeneMuenze;
        double rueckgabebetrag;
        int anzahlFahrkarten;
        double gesammtBetrag;

        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();

        // 2.Aufgabe
        System.out.println("Bitte tragen sie die Anzahl der Fahrkarten ein, die Sie erwerden wollen");
        anzahlFahrkarten = tastatur.nextInt();
        gesammtBetrag = zuZahlenderBetrag * anzahlFahrkarten;
        // Aufgabe 1
        System.out.printf("%s %.2f %n","Zu zahlender Betrag (EURO): ", gesammtBetrag);



        // Geldeinwurf
        // -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < gesammtBetrag)
        {
            //Aufgabe 1
            System.out.printf("%s %.2f %n","Noch zu zahlen: ", (gesammtBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        // RÃ¼ckgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgabebetrag = eingezahlterGesamtbetrag - gesammtBetrag;
        rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
        if(rueckgabebetrag > 0.0)
        {
            System.out.println("Der RÃ¼ckgabebetrag in HÃ¶he von " + rueckgabebetrag + " EURO");
            System.out.println("wird in folgenden MÃ¼nzen ausgezahlt:");


            while(rueckgabebetrag >= 2.0) // 2 EURO-MÃ¼nzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
                rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-MÃ¼nzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
                rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-MÃ¼nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
                rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
                rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
                rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
                rueckgabebetrag = Math.round(rueckgabebetrag * 100.0) / 100.0;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wÃ¼nschen Ihnen eine gute Fahrt.");
    }
}

