package Benutzerverwaltung;

import java.util.Scanner;

public class Benutzerverwaltung {

    public static void main(String[] args) {

        BenutzerverwaltungV10.start();

    }
}

class BenutzerverwaltungV10{
    public static void start(){
        BenutzerListe benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));

        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.
        menu(benutzerListe);

    }


    private static void menu(BenutzerListe bl) {
        System.out.println("================");
        System.out.println("1) Anmelden");
        System.out.println("2) Registrieren");
        System.out.println("================");
        Scanner input = new Scanner(System.in);
        int verifiedInput;

        while (true) {

            System.out.print("Bitte Wählen: ");
            String unverifiedInput = input.next();
            try {
                verifiedInput = Integer.parseInt(unverifiedInput);
                if (verifiedInput != 1) if (verifiedInput != 2) continue;
                else break; else break;

            } catch (Exception e) {

            }
        }

        if(verifiedInput == 1) {
            login(input, bl);
        } else if(verifiedInput == 2) {
            register(input, bl);
        } else {
            System.out.println("Diese Aktion gibt es nicht");
        }
    }

    private static void register(Scanner sc, BenutzerListe bl) {
        System.out.println("Geben Sie den gewuenschten Username ein:");
        String name = sc.next();
        if(bl.select(name) == "") {
            System.out.println("Geben Sie ihr Passwort ein: ");
            String pw1 = sc.next();

            System.out.println("Wiederholen Sie ihr Passwort ein:");
            String pw2 = sc.next();

            if(pw1.equals(pw2)) {

                bl.insert(new Benutzer(name, pw2));
                System.out.println("Ihr Benutzer wurde angelegt! \n Sie koennen Sich nun anmelden");
                login(sc, bl);
            } else {
                System.out.println("Die Passwoerter sind nicht identisch!");
                register(sc, bl);
            }
        } else {
            System.out.println("Der Benutzername ist schon vergeben!");
        }
    }

    private static void login(Scanner sc, BenutzerListe bl) {
        System.out.println("Geben Sie Ihren Nutzername an:");
        String name = sc.next();
        if(bl.select(name) != "") {
            checkPW(sc, bl, name);
        } else {
            System.out.println("Benutzer nicht gefunden:");
            login(sc, bl);
        }
    }

    private static int count = 0;
    private static void checkPW(Scanner sc, BenutzerListe bl, String name) {
        Benutzer b = bl.selectBenutzer(name);
        System.out.println("Benutzer gefunden \n Geben Sie Ihr Passwort ein:");
        String pw = sc.next();
        if(pw.equals(b.getPasswort())) {
            System.out.println("Sie sind nun eingeloggt als " + name);
        } else if(count < 2){
            System.out.println("Falsches Passwort");
            count++;
            checkPW(sc, bl, name);
        } else {
            System.out.println("Zu oft falsch eingegeben");
            menu(bl);
        }
    }


}

class BenutzerListe{
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public Benutzer selectBenutzer(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b;
            }
            b = b.getNext();
        }
        return null;
    }

    public boolean delete(String name){
        Benutzer b = first;
        String s = "";
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        b = null;
        return true;
    }
}

class Benutzer{
    private String name;
    private String passwort;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public String getName() {
        return name;
    }

    public String getPasswort() {
        return passwort;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}
