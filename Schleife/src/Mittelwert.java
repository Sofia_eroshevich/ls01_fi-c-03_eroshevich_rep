import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x;
      double y;
      double m;
      
      Scanner scan = new Scanner(System.in);
      
      System.out.print("Bitte geben Sie eine Zahl ein: ");
      x = scan.nextDouble();
  
      System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
      y = scan.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}
